class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    str.gsub(/[\w']+/) do |word|
      if word.length > 4 && word[0] == word[0].upcase
        "Marklar"
      elsif word.length > 4
        "marklar"
      else
        word
      end
    end
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    n1 = 0
    n2 = 1
    sum = 0

    (0...nth).each do
      sum += n2 if n2.even?
      temp = n2
      n2 = n1 + n2
      n1 = temp
    end

    sum
  end
end